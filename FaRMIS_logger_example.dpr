program FaRMIS_logger_example;

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {Main};

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'FaRMIS - logging example';
  Application.CreateForm(TMain, Main);
  Application.Run;
end.
