# FaRMIS logger example

### description

This is simple application for demonstration [QuickLogger](https://github.com/exilon/QuickLogger) and [QuickLib](https://github.com/exilon/QuickLib) logging library ...

### note

Remember, this is **ONLY** example and there are lot of bugs, dangerous parts of code, no validations, etc.