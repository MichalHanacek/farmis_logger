unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, Vcl.ExtCtrls, Vcl.ComCtrls,
  Quick.Logger, Quick.Logger.Provider.Files, Quick.Logger.Provider.Console, Quick.Logger.Provider.Email,
  Vcl.ButtonGroup, System.Actions, Vcl.ActnList;

type
  TMain = class(TForm)
    Email: TGroupBox;
    email_smtp: TGroupBox;
    smtp_host_LabeledEdit: TLabeledEdit;
    smtp_port_LabeledEdit: TLabeledEdit;
    smtp_username_LabeledEdit: TLabeledEdit;
    smtp_password_LabeledEdit: TLabeledEdit;
    smtp_serverAuth_CheckBox: TCheckBox;
    smtp_useSSL_CheckBox: TCheckBox;
    email_endpoint: TGroupBox;
    endpoint_from_LabeledEdit: TLabeledEdit;
    endpoint_recipient_LabeledEdit: TLabeledEdit;
    endpoint_subject_LabeledEdit: TLabeledEdit;
    PageControl1: TPageControl;
    settings_TabSheet: TTabSheet;
    app_TabSheet: TTabSheet;
    email_useEmailLogging_CheckBox: TCheckBox;
    file_GroupBox: TGroupBox;
    file_useFileLogging_CheckBox: TCheckBox;
    file_fileName_LabeledEdit: TLabeledEdit;
    email_logLevel_ComboBox: TComboBox;
    email_logLevel_Label: TLabel;
    file_logLevel_Label: TLabel;
    file_logLevel_ComboBox: TComboBox;
    settings_intoApp_Button: TButton;
    ButtonGroup1: TButtonGroup;
    ActionList: TActionList;
    Log_etHeader_Action: TAction;
    Log_etInfo_Action: TAction;
    Log_etSuccess_Action: TAction;
    Log_etWarning_Action: TAction;
    Log_etError_Action: TAction;
    Log_etCritical_Action: TAction;
    Log_etException_Action: TAction;
    Log_etDebug_Action: TAction;
    Log_etTrace_Action: TAction;
    Log_etDone_Action: TAction;
    Log_etCustom1_Action: TAction;
    Log_etCustom2_Action: TAction;
    procedure FormShow(Sender: TObject);
    procedure settings_intoApp_ButtonClick(Sender: TObject);
    procedure Log_etHeader_ActionExecute(Sender: TObject);
    procedure Log_etInfo_ActionExecute(Sender: TObject);
    procedure Log_etSuccess_ActionExecute(Sender: TObject);
    procedure Log_etWarning_ActionExecute(Sender: TObject);
    procedure Log_etError_ActionExecute(Sender: TObject);
    procedure Log_etCritical_ActionExecute(Sender: TObject);
    procedure Log_etException_ActionExecute(Sender: TObject);
    procedure Log_etDebug_ActionExecute(Sender: TObject);
    procedure Log_etTrace_ActionExecute(Sender: TObject);
    procedure Log_etDone_ActionExecute(Sender: TObject);
    procedure Log_etCustom1_ActionExecute(Sender: TObject);
    procedure Log_etCustom2_ActionExecute(Sender: TObject);
  private
    MySMTP: Quick.Logger.Provider.Email.TSMTPConfig;
    MyMAIL: Quick.Logger.Provider.Email.TMailConfig;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Main: TMain;

implementation

{$R *.dfm}

uses
  IniFiles;

procedure TMain.FormShow(Sender: TObject);
var
  IniFile: TIniFile;
begin
  app_TabSheet.PageControl := NIL;

  Self.PageControl1.ActivePage := settings_TabSheet;

  // loading last settings
  IniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    // file
    file_useFileLogging_CheckBox.Checked := IniFile.ReadBool('FILE', 'USE', False);
    file_fileName_LabeledEdit.Text := IniFile.ReadString('FILE', 'FILENAME', '');
    file_logLevel_ComboBox.ItemIndex := IniFile.ReadInteger('FILE', 'LOGLEVEL', 3);
    // email
    email_useEmailLogging_CheckBox.Checked := IniFile.ReadBool('EMAIL', 'USE', False);
    smtp_host_LabeledEdit.Text := IniFile.ReadString('EMAIL', 'SMTP_HOST', '');
    smtp_port_LabeledEdit.Text := IniFile.ReadString('EMAIL', 'SMTP_PORT', '465');
    smtp_username_LabeledEdit.Text := IniFile.ReadString('EMAIL', 'SMTP_USERNAME', '');
    smtp_password_LabeledEdit.Text := IniFile.ReadString('EMAIL', 'SMTP_PASSWORD', '');
    smtp_serverAuth_CheckBox.Checked := IniFile.ReadBool('EMAIL', 'SMTP_SERVERAUTH', False);
    smtp_useSSL_CheckBox.Checked := IniFile.ReadBool('EMAIL', 'SMTP_USESSL', False);
    endpoint_from_LabeledEdit.Text := IniFile.ReadString('EMAIL', 'ENDPOINT_FROM', '');
    endpoint_recipient_LabeledEdit.Text := IniFile.ReadString('EMAIL', 'ENDPOINT_RECIPIENT', '');
    endpoint_subject_LabeledEdit.Text := IniFile.ReadString('EMAIL', 'ENDPOINT_SUBJECT', '');
    email_logLevel_ComboBox.ItemIndex := IniFile.ReadInteger('EMAIL', 'LOGLEVEL', 3);
  finally
    FreeAndNil(IniFile);
  end;
end;

procedure TMain.Log_etCritical_ActionExecute(Sender: TObject);
begin
  Log('Test LOG critical', etCritical);
end;

procedure TMain.Log_etCustom1_ActionExecute(Sender: TObject);
begin
  Log('Test LOG custom1', etCustom1);
end;

procedure TMain.Log_etCustom2_ActionExecute(Sender: TObject);
begin
  Log('Test LOG custom2', etCustom2);
end;

procedure TMain.Log_etDebug_ActionExecute(Sender: TObject);
begin
  Log('Test LOG debug', etDebug);
end;

procedure TMain.Log_etDone_ActionExecute(Sender: TObject);
begin
  Log('Test LOG done', etDone);
end;

procedure TMain.Log_etError_ActionExecute(Sender: TObject);
begin
  Log('Test LOG error', etError);
end;

procedure TMain.Log_etException_ActionExecute(Sender: TObject);
begin
  Log('Test LOG exception', etException);
end;

procedure TMain.Log_etHeader_ActionExecute(Sender: TObject);
begin
  Log('Test LOG header', etHeader);
end;

procedure TMain.Log_etInfo_ActionExecute(Sender: TObject);
begin
  Log('Test LOG info', etInfo);
end;

procedure TMain.Log_etSuccess_ActionExecute(Sender: TObject);
begin
  Log('Test LOG success', etSuccess);
end;

procedure TMain.Log_etTrace_ActionExecute(Sender: TObject);
begin
  Log('Test LOG trace', etTrace);
end;

procedure TMain.Log_etWarning_ActionExecute(Sender: TObject);
begin
  Log('Test LOG warning', etWarning);
end;

procedure TMain.settings_intoApp_ButtonClick(Sender: TObject);
var
  IniFile: TIniFile;
begin
  // POZN_MH: this is a example, i need easy and simple solution ... i dont solve validations and exeptions

  // save actual last settings
  IniFile := TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini'));
  try
    IniFile.WriteBool('FILE', 'USE', file_useFileLogging_CheckBox.Checked);
    if file_useFileLogging_CheckBox.Checked then
    begin
      Logger.Providers.Add(GlobalLogFileProvider);

      with GlobalLogFileProvider do
      begin
        if Length(file_fileName_LabeledEdit.Text) > 0 then
          FileName := file_fileName_LabeledEdit.Text
        else
          FileName := '.\Logger.log';

        case file_logLevel_ComboBox.ItemIndex of
          0:
            LogLevel := LOG_ONLYERRORS;
          1:
            LogLevel := LOG_ERRORSANDWARNINGS;
          2:
            LogLevel := LOG_BASIC;
          3:
            LogLevel := LOG_ALL;
          4:
            LogLevel := LOG_TRACE;
          5:
            LogLevel := LOG_DEBUG;
          else
            LogLevel := LOG_ALL;
        end;

        DailyRotate := True;
        MaxFileSizeInMB := 20;
        Enabled := True;

        // file
        IniFile.WriteString('FILE', 'FILENAME', FileName);
        IniFile.WriteInteger('FILE', 'LOGLEVEL', file_logLevel_ComboBox.ItemIndex);
      end;
    end;

    IniFile.WriteBool('EMAIL', 'USE', email_useEmailLogging_CheckBox.Checked);
    if email_useEmailLogging_CheckBox.Checked then
    begin
      Logger.Providers.Add(GlobalLogEmailProvider);

      // Configure email provider options
      with GlobalLogEmailProvider do
      begin
        MySMTP := Quick.Logger.Provider.Email.TSMTPConfig.Create;
        MySMTP.Host := smtp_host_LabeledEdit.Text;
        MySMTP.Port := StrToIntDef(smtp_port_LabeledEdit.Text, 465);
        MySMTP.UserName := smtp_username_LabeledEdit.Text;
        MySMTP.Password := smtp_password_LabeledEdit.Text;
        MySMTP.ServerAuth := smtp_serverAuth_CheckBox.Checked;
        MySMTP.UseSSL := smtp_useSSL_CheckBox.Checked;

        MyMAIL := Quick.Logger.Provider.Email.TMailConfig.Create;
        MyMAIL.From := endpoint_from_LabeledEdit.Text;
        MyMAIL.Recipient := endpoint_recipient_LabeledEdit.Text;
        MyMAIL.Subject := endpoint_subject_LabeledEdit.Text;
        // MyMAIL.SenderName :=  '';
        // MyMAIL.Body :=  '';
        // MyMAIL.CC := '';
        // MyMAIL.BCC := '';}

        case email_logLevel_ComboBox.ItemIndex of
          0:
            LogLevel := LOG_ONLYERRORS;
          1:
            LogLevel := LOG_ERRORSANDWARNINGS;
          2:
            LogLevel := LOG_BASIC;
          3:
            LogLevel := LOG_ALL;
          4:
            LogLevel := LOG_TRACE;
          5:
            LogLevel := LOG_DEBUG;
          else
            LogLevel := LOG_ALL;
        end;

        SMTP := MySMTP;
        Mail := MyMAIL;
        Enabled := True;

        // email
        IniFile.WriteString('EMAIL', 'SMTP_HOST', MySMTP.Host);
        IniFile.WriteInteger('EMAIL', 'SMTP_PORT', MySMTP.Port);
        IniFile.WriteString('EMAIL', 'SMTP_USERNAME', MySMTP.UserName);
        IniFile.WriteString('EMAIL', 'SMTP_PASSWORD', MySMTP.Password);
        IniFile.WriteBool('EMAIL', 'SMTP_SERVERAUTH', MySMTP.ServerAuth);
        IniFile.WriteBool('EMAIL', 'SMTP_USESSL', MySMTP.UseSSL);

        IniFile.WriteString('EMAIL', 'ENDPOINT_FROM', MyMAIL.From);
        IniFile.WriteString('EMAIL', 'ENDPOINT_RECIPIENT', MyMAIL.Recipient);
        IniFile.WriteString('EMAIL', 'ENDPOINT_SUBJECT', MyMAIL.Subject);

        IniFile.WriteInteger('FILE', 'LOGLEVEL', email_logLevel_ComboBox.ItemIndex);
      end;
    end;

    // Configure console provider options
    Logger.Providers.Add(GlobalLogConsoleProvider);
    with GlobalLogConsoleProvider do
    begin
      LogLevel := LOG_DEBUG;
      ShowEventColors := True;
      Enabled := True;
    end;
  finally
    FreeAndNil(IniFile);
  end;


  app_TabSheet.PageControl := Self.PageControl1;
  Self.PageControl1.ActivePage := app_TabSheet;
  settings_TabSheet.PageControl := NIL;
end;

end.
