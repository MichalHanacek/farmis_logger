object Main: TMain
  Left = 0
  Top = 0
  Caption = 'FaRMIS - logger example'
  ClientHeight = 347
  ClientWidth = 1073
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl1: TPageControl
    Left = 0
    Top = 0
    Width = 1073
    Height = 347
    ActivePage = app_TabSheet
    Align = alClient
    TabOrder = 0
    object settings_TabSheet: TTabSheet
      Caption = 'settings'
      DesignSize = (
        1065
        319)
      object email: TGroupBox
        Left = 0
        Top = 9
        Width = 369
        Height = 309
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = 'email'
        TabOrder = 0
        DesignSize = (
          369
          309)
        object email_logLevel_Label: TLabel
          Left = 176
          Top = 17
          Width = 39
          Height = 13
          Caption = 'logLevel'
        end
        object email_endpoint: TGroupBox
          Left = 192
          Top = 36
          Width = 174
          Height = 269
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = 'endpoint'
          TabOrder = 0
          object endpoint_from_LabeledEdit: TLabeledEdit
            Left = 3
            Top = 32
            Width = 168
            Height = 21
            EditLabel.Width = 22
            EditLabel.Height = 13
            EditLabel.Caption = 'from'
            TabOrder = 0
          end
          object endpoint_recipient_LabeledEdit: TLabeledEdit
            Left = 3
            Top = 74
            Width = 168
            Height = 21
            EditLabel.Width = 41
            EditLabel.Height = 13
            EditLabel.Caption = 'recipient'
            TabOrder = 1
          end
          object endpoint_subject_LabeledEdit: TLabeledEdit
            Left = 3
            Top = 114
            Width = 168
            Height = 21
            EditLabel.Width = 35
            EditLabel.Height = 13
            EditLabel.Caption = 'subject'
            TabOrder = 2
          end
        end
        object email_smtp: TGroupBox
          Left = 3
          Top = 36
          Width = 174
          Height = 269
          Anchors = [akLeft, akTop, akRight, akBottom]
          Caption = 'smtp'
          TabOrder = 1
          object smtp_host_LabeledEdit: TLabeledEdit
            Left = 3
            Top = 32
            Width = 168
            Height = 21
            EditLabel.Width = 21
            EditLabel.Height = 13
            EditLabel.Caption = 'host'
            TabOrder = 0
          end
          object smtp_port_LabeledEdit: TLabeledEdit
            Left = 3
            Top = 72
            Width = 168
            Height = 21
            EditLabel.Width = 20
            EditLabel.Height = 13
            EditLabel.Caption = 'port'
            TabOrder = 1
          end
          object smtp_username_LabeledEdit: TLabeledEdit
            Left = 3
            Top = 114
            Width = 168
            Height = 21
            EditLabel.Width = 47
            EditLabel.Height = 13
            EditLabel.Caption = 'username'
            TabOrder = 2
          end
          object smtp_password_LabeledEdit: TLabeledEdit
            Left = 3
            Top = 154
            Width = 168
            Height = 21
            EditLabel.Width = 46
            EditLabel.Height = 13
            EditLabel.Caption = 'password'
            TabOrder = 3
          end
          object smtp_serverAuth_CheckBox: TCheckBox
            Left = 3
            Top = 192
            Width = 168
            Height = 17
            Caption = 'serverAuth'
            TabOrder = 4
          end
          object smtp_useSSL_CheckBox: TCheckBox
            Left = 3
            Top = 215
            Width = 168
            Height = 17
            Caption = 'use SSL'
            TabOrder = 5
          end
        end
        object email_useEmailLogging_CheckBox: TCheckBox
          Left = 6
          Top = 16
          Width = 155
          Height = 17
          Caption = 'use logging into email'
          TabOrder = 2
        end
        object email_logLevel_ComboBox: TComboBox
          Left = 221
          Top = 14
          Width = 145
          Height = 21
          ItemIndex = 3
          TabOrder = 3
          Text = 'LOG_ALL'
          Items.Strings = (
            'LOG_ONLYERRORS'
            'LOG_ERRORSANDWARNINGS'
            'LOG_BASIC'
            'LOG_ALL'
            'LOG_TRACE'
            'LOG_DEBUG')
        end
      end
      object file_GroupBox: TGroupBox
        Left = 375
        Top = 9
        Width = 369
        Height = 309
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = 'file'
        TabOrder = 1
        object file_logLevel_Label: TLabel
          Left = 181
          Top = 17
          Width = 39
          Height = 13
          Caption = 'logLevel'
        end
        object file_useFileLogging_CheckBox: TCheckBox
          Left = 6
          Top = 16
          Width = 180
          Height = 17
          Caption = 'use logging into file'
          TabOrder = 0
        end
        object file_fileName_LabeledEdit: TLabeledEdit
          Left = 6
          Top = 68
          Width = 168
          Height = 21
          EditLabel.Width = 14
          EditLabel.Height = 13
          EditLabel.Caption = 'file'
          TabOrder = 1
        end
        object file_logLevel_ComboBox: TComboBox
          Left = 221
          Top = 14
          Width = 145
          Height = 21
          ItemIndex = 3
          TabOrder = 2
          Text = 'LOG_ALL'
          Items.Strings = (
            'LOG_ONLYERRORS'
            'LOG_ERRORSANDWARNINGS'
            'LOG_BASIC'
            'LOG_ALL'
            'LOG_TRACE'
            'LOG_DEBUG')
        end
      end
      object settings_intoApp_Button: TButton
        Left = 750
        Top = 9
        Width = 312
        Height = 309
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = 'into App'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -40
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
        OnClick = settings_intoApp_ButtonClick
      end
    end
    object app_TabSheet: TTabSheet
      Margins.Left = 10
      Margins.Top = 10
      Margins.Right = 10
      Margins.Bottom = 10
      Caption = 'app'
      ImageIndex = 1
      object ButtonGroup1: TButtonGroup
        Left = 0
        Top = 0
        Width = 1065
        Height = 319
        Align = alClient
        ButtonOptions = [gboAllowReorder, gboFullSize, gboGroupStyle, gboShowCaptions]
        Items = <
          item
            Action = Log_etHeader_Action
          end
          item
            Action = Log_etInfo_Action
          end
          item
            Action = Log_etSuccess_Action
          end
          item
            Action = Log_etWarning_Action
          end
          item
            Action = Log_etError_Action
          end
          item
            Action = Log_etCritical_Action
          end
          item
            Action = Log_etException_Action
          end
          item
            Action = Log_etDebug_Action
          end
          item
            Action = Log_etTrace_Action
          end
          item
            Action = Log_etDone_Action
          end
          item
            Action = Log_etCustom1_Action
          end
          item
            Action = Log_etCustom2_Action
          end>
        TabOrder = 0
        ExplicitLeft = 240
        ExplicitTop = 128
        ExplicitWidth = 100
        ExplicitHeight = 100
      end
    end
  end
  object ActionList: TActionList
    Left = 1020
    Top = 32
    object Log_etHeader_Action: TAction
      Caption = 'Header'
      OnExecute = Log_etHeader_ActionExecute
    end
    object Log_etInfo_Action: TAction
      Caption = 'Info'
      OnExecute = Log_etInfo_ActionExecute
    end
    object Log_etSuccess_Action: TAction
      Caption = 'Success'
      OnExecute = Log_etSuccess_ActionExecute
    end
    object Log_etWarning_Action: TAction
      Caption = 'Warning'
      OnExecute = Log_etWarning_ActionExecute
    end
    object Log_etError_Action: TAction
      Caption = 'Error'
      OnExecute = Log_etError_ActionExecute
    end
    object Log_etCritical_Action: TAction
      Caption = 'Critical'
      OnExecute = Log_etCritical_ActionExecute
    end
    object Log_etException_Action: TAction
      Caption = 'Exception'
      OnExecute = Log_etException_ActionExecute
    end
    object Log_etDebug_Action: TAction
      Caption = 'Debug'
      OnExecute = Log_etDebug_ActionExecute
    end
    object Log_etTrace_Action: TAction
      Caption = 'Trace'
      OnExecute = Log_etTrace_ActionExecute
    end
    object Log_etDone_Action: TAction
      Caption = 'Done'
      OnExecute = Log_etDone_ActionExecute
    end
    object Log_etCustom1_Action: TAction
      Caption = 'Custom1'
      OnExecute = Log_etCustom1_ActionExecute
    end
    object Log_etCustom2_Action: TAction
      Caption = 'Custom2'
      OnExecute = Log_etCustom2_ActionExecute
    end
  end
end
